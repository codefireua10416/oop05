/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction03;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Car car1 = new Car(BodyType.AUTO);
        
        switch (car1.getBodyType()) {
            case AUTO:
                break;
            case BUS:
                break;
            case JEEP:
                break;
        }
    }
    
}
