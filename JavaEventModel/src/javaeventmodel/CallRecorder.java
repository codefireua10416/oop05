/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaeventmodel;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class CallRecorder implements CallListener {

    @Override
    public void actionCall(String phone) {
        System.out.println("START record call: " + phone);
        System.out.println(">>>");
        
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException ex) {
            Logger.getLogger(Phone.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        System.out.println("<<<");
        System.out.println("STOP record call: " + phone);
    }
    
}
