/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaeventmodel;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author human
 */
public class Phone {

    private CallListener listener;

    public CallListener getListener() {
        return listener;
    }

    public void setListener(CallListener listener) {
        this.listener = listener;
    }

    public void callTo(String phone) {
        System.out.println("Calling to: " + phone);

        // EVENT
        if (callReceived()) {
            if (listener != null) {
                listener.actionCall(phone);
            }
        }

        System.out.println("End call to: " + phone);
    }

    private boolean callReceived() {
        try {
            Thread.sleep((long) (Math.random() * 10000));
        } catch (InterruptedException ex) {
            Logger.getLogger(Phone.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
