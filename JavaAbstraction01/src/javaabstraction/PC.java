/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public class PC extends Computer {

    @Override
    public void reboot() {
        System.out.println("Turn off");
        if (turn(false)) {
            System.out.println("reset");
            System.out.println("Turn on");
            turn(true);
        }
    }

}
