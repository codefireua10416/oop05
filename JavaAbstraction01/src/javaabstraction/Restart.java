/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public interface Restart extends Power {

    /**
     * Restart system.
     */
    void reboot();
}
