/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public abstract class Computer implements Power, Restart {

    private boolean powerState; // false

    public boolean isPowerState() {
        return powerState;
    }

    @Override
    public boolean turn(boolean state) {
        if (!powerState && state) {
            powerState = true;
            System.out.println("Change state to on");
            return true;
        } else if (powerState && !state) {
            powerState = false;
            System.out.println("Change state to off");
            return true;
        }
        
        System.out.println("State not changed!");
        return false;
    }

}
