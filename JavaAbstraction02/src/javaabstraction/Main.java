/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ship ship1 = new Ship(3);
        ship1.setFuel(100);

        for (int i = 1; !ship1.isEmptyFuel(); i++) {
            ship1.forward();
            System.out.printf("%03d Rest: %05d\n", i, ship1.getFuel());
        }
    }

}
