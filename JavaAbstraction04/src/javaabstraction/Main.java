/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Ship ship1 = new Ship(3);
        ship1.setFuel(100);

        ship1.move(MoveDirection.UP);

        for (int i = 1; !ship1.isEmptyFuel(); i++) {
            ship1.move(MoveDirection.FORWARD);
            
            // BAD CODE...
//            if (ship1.getFuel() % 3 == 0) {
//                ship1.setFuel(100);
//            }
            
            System.out.printf("%03d Rest: %05d\n", i, ship1.getFuel());
        }

    }

}
