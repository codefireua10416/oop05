/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaabstraction;

/**
 *
 * @author human
 */
public class Ship extends Machine {

    private int fuel; // 0
    private int consumption;

    public Ship(int consumption) {
        this.consumption = consumption;
    }

    public int getConsumption() {
        return consumption;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    /**
     * Return boolean describes fuel state.
     *
     * @return true if empty, otherwise false.
     */
    public boolean isEmptyFuel() {
        return fuel <= 0;
    }

    @Override
    public void move(MoveDirection direction) {
        if (fuel > 0) {
            switch (direction) {
                case FORWARD:
                    System.out.println("Swim forward");
                    break;
                case BACKWARD:
                    System.out.println("Swim backward");
                    break;
                case LEFT:
                    System.out.println("Turn left");
                    break;
                case RIGHT:
                    System.out.println("Turn right");
                    break;
                default:
                    System.out.println("ERROR DIRECTION " + direction);
                    break;
            }
            
            fuel -= consumption;

            if (fuel < 0) {
                fuel = 0;
            }
        }
    }

}
